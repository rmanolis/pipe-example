import {
  Source,
  source,
  PinLike,
  composition,
  pin,
  sink,
  filter,
  map,
  handleError,
  Emission
} from "@connectv/core";
import { Assertable } from "../interfaces/assertable.interface";
import { PipeAssertion } from "./pipe-assertion.controller";
import { Log } from "../interfaces/log.interface";
/*
Create pipe for each new request-id that comes.
The pipe filters the service and method to run the assertions
Delete the pipe when the request-id end.
The request-id ends when the ordering ends
If there is not any ordering in the scripts then it will not create a pipe.
If there is a method in the ordering, it will return an error message

Map source with request-id
*/

export class SourceMapController {
  // map request_ids with sources
  private sources: Map<String, Source> = new Map<String, Source>();
  private errorMessages: string[] = [];

  constructor(private assertions: Assertable[]) {}
  addRequestId(req_id: string) {
    if (!this.sources.has(req_id)) {
      this.sources.set(req_id, source());
    }
  }

  getErrorMessages() {
    return this.errorMessages;
  }

  getSource(req_id: string): Source | undefined {
    return this.sources.get(req_id);
  }

  sendLog(log: Log) {
    let source = this.getSource(log.request_id);
    if (source === undefined) {
      throw new Error("Source is missing");
    }
    source!.send(log);
  }

  subscribe(req_id: string) {
    this.addRequestId(req_id);
    let source = this.sources.get(req_id);
    if (!source) {
      console.log(
        "Error: failed to find the source for request id is " + req_id
      );
      return;
    }
    let p: PinLike = source;
    for (const script of this.assertions) {
      // const pac = new PipeAgentController(script);
      // p = p.to(pac);
      p = p.to(this.assert(script)());
    }
    p.subscribe(() => {});
  }

  remove(req_id: string) {
    const s = this.getSource(req_id);
    if (s !== undefined) {
      s.clear();
      this.sources.delete(req_id);
    }
  }

  private assert(assertion: Assertable) {
    const pipeAssertion = new PipeAssertion(assertion);
    return composition(track => {
      const logger = pin();
      const out = pin();
      const pushError = sink((x: any) => {
        this.errorMessages.push(x.message);
      });
      const findCrash = (x: any) => {
        const log = x as Log;
        if (log.dataType === "crash") {
          throw new Error(
            `request ${log.request_id}: The service ${log.service} crashed on method ${log.method}`
          );
        }
        return x;
      };
      logger
        .to(filter((x: any) => pipeAssertion.filterMap(x)))
        .to(sink((x: any) => pipeAssertion.methodMap(x)))
        .to(sink((x: any) => pipeAssertion.testLogMap(x)))
        .to(sink((x: any) => pipeAssertion.actionMap(x)))
        .to(sink(findCrash))
        .to(handleError())
        .serialTo(pin(), pushError)
        .to(out);
      return [{ logger }, { out }];
    });
  }
}
