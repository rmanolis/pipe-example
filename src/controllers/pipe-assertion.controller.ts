import { Assertable } from "../interfaces/assertable.interface";
import { Log } from "../interfaces/log.interface";
import * as _ from "lodash";
import * as chai from "chai";

export class PipeAssertion {
  constructor(private assertion: Assertable) {}

  filterMap(log: Log): boolean {
    const reqRx = new RegExp(this.assertion.reqIdRx);
    const methodRx = new RegExp(this.assertion.methodRx);
    const serviceRx = new RegExp(this.assertion.serviceRx);
    return (
      reqRx.test(log.request_id) &&
      methodRx.test(log.method) &&
      serviceRx.test(log.service)
    );
  }

  /*
   * Throw errors:
   * - If a method or service exist in the list, throw an error
   */
  methodMap(log: Log): Log {
    const obj = _.find(this.assertion.methods, {
      service: log.service,
      method: log.method
    });
    if (!obj) {
      throw new Error(
        `request ${log.request_id}: ${this.assertion.title} on ordering is missing the method ${log.method} from service ${log.service}`
      );
    }
    return log;
  }

  actionMap(log: Log): Log {
    for (const action of this.assertion.actions) {
      try {
        action.assert(log);
      } catch (e) {
        action.actOnFail();
        throw new Error(
          `request ${log.request_id}: The assertion with title "${action.title}" failed because of "${e.message}"`
        );
      }
    }
    return log;
  }

  testLogMap(log: Log): Log {
    for (const testLog of this.assertion.testLogs) {
      if (
        log.request_id === testLog.request_id &&
        log.service === testLog.service &&
        log.method === testLog.method &&
        log.dataType === testLog.dataType
      ) {
        chai.expect(testLog.data).to.deep.equal(log.data);
      }
    }
    return log;
  }
}
