import { Agent, map, filter } from "@connectv/core";
import { Assertable } from "../interfaces/assertable.interface";
import { Log } from "../interfaces/log.interface";
import * as _ from "lodash";

export class PipeAgentController extends Agent {
  constructor(private assertion: Assertable) {
    super({
      inputs: ["in"],
      outputs: ["out"]
    });
    this.in("in")
      .to(
        filter((x: any) => {
          console.log("filter ", x);
          return this.filterMap(x);
        })
      )
      .to(
        map((x: any) => {
          return this.orderMap(x);
        })
      )
      .to(
        map((x: any) => {
          return this.actionMap(x);
        })
      )
      .to(
        map((x: any) => {
          return this.assertionMap(x);
        })
      )
      .to(this.out("out"));
  }
  createEntries() {
    return [this.in("in")];
  }
  createExits() {
    return [this.out("out")];
  }
  private filterMap(log: Log): boolean {
    const reqRx = new RegExp(this.assertion!.reqIdRx);
    const methodRx = new RegExp(this.assertion!.methodRx);
    const serviceRx = new RegExp(this.assertion!.serviceRx);
    return (
      reqRx.test(log.request_id) &&
      methodRx.test(log.method) &&
      serviceRx.test(log.service)
    );
  }

  private orderMap(log: Log): Log {
    const obj = _.find(this.assertion!.ordering, {
      service: log.service,
      method: log.method
    });
    if (obj) {
      console.log("Passed : ", log);
    }
    return log;
  }

  private actionMap(log: Log): Log {
    return log;
  }

  private assertionMap(log: Log): Log {
    return log;
  }
}
