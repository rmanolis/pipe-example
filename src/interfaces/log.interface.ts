export interface Log {
  service: string;
  method: string;
  request_id: string;
  dataType: "input" | "output" | "error" | "crash";
  data: any;
}
