import { Log } from "./log.interface";

export type Assertion = (data: Log) => void;
export type Method = {
  service: string;
  method: string;
};
export type Action = {
  title: string;
  assert: Assertion;
  actOnFail: () => void;
};

export interface Assertable {
  title: string;
  serviceRx: string;
  methodRx: string;
  reqIdRx: string;
  testLogs: Log[]; // list of specific logs to pass
  methods: Method[]; // methods to call
  actions: Action[]; // list of actions after failure
}
