import * as util from "util";
import * as fs from "fs";
import * as path from "path";
import { Assertable } from "./interfaces/assertable.interface";
import { SourceMapController } from "./controllers/source-map.controller";
import { Log } from "./interfaces/log.interface";

const readFile = util.promisify(fs.readFile);
const readdir = util.promisify(fs.readdir);

const assetionsPath = path.join(__dirname, "..", "assertions");
const jsonsPath = path.join(__dirname, "..", "jsons");
async function main() {
  console.log("Starting service");
  console.log(`Read files from paths:
    assertions: ${assetionsPath}`);
  const scripts = await readdir(assetionsPath);
  const assertions: Assertable[] = [];
  for (const script of scripts) {
    const executable = await import(path.join(assetionsPath, script));
    const assertion: Assertable = executable.default;
    assertions.push(assertion);
  }
  const sourceMap = new SourceMapController(assertions);

  const jsons = await readdir(jsonsPath);
  for (const jsonFile of jsons) {
    const file = await readFile(path.join(jsonsPath, jsonFile), "utf8");
    const logs: Log[] = JSON.parse(file);
    for (const log of logs) {
      if (sourceMap.getSource(log.request_id) === undefined) {
        sourceMap.subscribe(log.request_id);
      }
      sourceMap.sendLog(log);
    }
  }
}

main();
