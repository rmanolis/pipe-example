import { SourceMapController } from "../src/controllers/source-map.controller";
import { Method, Assertable } from "../src/interfaces/assertable.interface";
import { Log } from "../src/interfaces/log.interface";

const logs: Log[] = [
  {
    service: "api",
    method: "callCreateInstance",
    request_id: "instance-creation",
    dataType: "input",
    data: { size: 5, cpu: 1, ram: 512 }
  },
  {
    service: "api",
    method: "startCreatingInstance",
    request_id: "instance-creation",
    dataType: "input",
    data: { size: 5, cpu: 1, ram: 512 }
  },
  {
    service: "storage",
    method: "startCreatingVolume",
    request_id: "instance-creation",
    dataType: "input",
    data: { size: 5 }
  },
  {
    service: "storage",
    method: "startCreatingVolume",
    request_id: "instance-creation",
    dataType: "output",
    data: { volume_id: "volume-1" }
  },
  {
    service: "virt",
    method: "startCreatingVM",
    request_id: "instance-creation",
    dataType: "input",
    data: { volume_id: "volume-1", cpu: 1, ram: 512 }
  },
  {
    service: "hypervisor",
    method: "startCreatingVM",
    request_id: "instance-creation",
    dataType: "input",
    data: { volume_id: "volume-1", cpu: 1, ram: 512 }
  },
  {
    service: "hypervisor",
    method: "startCreatingVM",
    request_id: "instance-creation",
    dataType: "output",
    data: { instance_id: "instance-1" }
  },
  {
    service: "virt",
    method: "startCreatingVM",
    request_id: "instance-creation",
    dataType: "output",
    data: { instance_id: "instance-1" }
  },
  {
    service: "api",
    method: "startCreatingInstance",
    request_id: "instance-creation",
    dataType: "output",
    data: { instance_id: "instance-1" }
  },
  {
    service: "api",
    method: "callCreateInstance",
    request_id: "instance-creation",
    dataType: "output",
    data: { instance_id: "instance-1" }
  }
];

const methods: Method[] = [
  {
    service: "api",
    method: "callCreateInstance"
  },
  {
    service: "api",
    method: "startCreatingInstance"
  },
  {
    service: "storage",
    method: "startCreatingVolume"
  },
  {
    service: "virt",
    method: "startCreatingVM"
  },
  {
    service: "hypervisor",
    method: "startCreatingVM"
  }
];
const assertion: Assertable = {
  title: "Instance Creation",
  serviceRx: "\\w+",
  methodRx: "\\w+",
  reqIdRx: "instance-creation",
  testLogs: [],
  actions: [],
  methods
};

test("instance-creation checking methods success", () => {
  const copiedAssertion = Object.assign({}, assertion);
  const assertions = [copiedAssertion];
  const sourceMap = new SourceMapController(assertions);

  for (const log of logs) {
    if (sourceMap.getSource(log.request_id) === undefined) {
      sourceMap.subscribe(log.request_id);
    }
    sourceMap.sendLog(log);
  }
  expect(sourceMap.getErrorMessages().length).toEqual(0);
});

test("instance-creation checking methods failed", () => {
  const copiedAssertion = Object.assign({}, assertion);
  // remove only one method that is called twice from the logs
  copiedAssertion.methods = copiedAssertion.methods.filter(
    x => x.method !== "startCreatingVM" || x.service !== "virt"
  );
  const assertions = [copiedAssertion];
  const sourceMap = new SourceMapController(assertions);

  for (const log of logs) {
    if (sourceMap.getSource(log.request_id) === undefined) {
      sourceMap.subscribe(log.request_id);
    }
    sourceMap.sendLog(log);
  }
  expect(sourceMap.getErrorMessages().length).toEqual(2);
});
