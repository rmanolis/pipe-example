import { OrderItem } from "../src/interfaces/assertable.interface";
import { Log } from "../src/interfaces/log.interface";
const ordering: OrderItem[] = [
  {
    service: "api",
    method: "callCreateInstance"
  },
  {
    service: "api",
    method: "startCreatingInstance"
  },
  {
    service: "storage",
    method: "startCreatingVolume"
  },
  {
    service: "virt",
    method: "startCreatingVM"
  },
  {
    service: "hypervisor",
    method: "startCreatingVM"
  }
];
export default {
  serviceRx: "\\w+",
  methodRx: "\\w+",
  reqIdRx: "instance-creation",
  testLogs: [],
  actions: [],
  ordering
};
